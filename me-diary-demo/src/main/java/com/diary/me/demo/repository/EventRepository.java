package com.diary.me.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.diary.me.demo.entity.Event;

public interface EventRepository
        extends PagingAndSortingRepository<Event, Long>
{
	Page<Event> findByPatientId(@Param("patientId") Long patientId, Pageable pageable);	
}
