package com.diary.me.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.diary.me.demo.entity.Patient;

public interface PatientRepository
        extends PagingAndSortingRepository<Patient, Long>
{
	Page<Patient> findByFirstName(@Param("firstName") String firstName,
	                              Pageable pageable);

	Page<Patient> findByLastName(@Param("lastName") String lastName,
	                             Pageable pageable);
	
	Page<Patient> findByFirstNameAndLastName(@Param("firstName") String firstName, @Param("lastName") String lastName,
	                              Pageable pageable);

	Page<Patient> findByEmail(@Param("email") String email, Pageable pageable);

	Page<Patient> findByPhone(@Param("phone") String phone, Pageable pageable);

	Page<Patient> findByDateOfBirth(@Param("dateOfBirth") String dateOfBirth,
	                                Pageable pageable);
}
