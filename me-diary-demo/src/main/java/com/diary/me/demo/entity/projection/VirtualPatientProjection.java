package com.diary.me.demo.entity.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.diary.me.demo.entity.Patient;

@Projection(name="virtual", types = {Patient.class})
public interface VirtualPatientProjection {

	@Value("#{target.firstName} #{target.lastName}")
	String getName();
}
