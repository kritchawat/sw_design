package com.diary.me.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.diary.me.demo.entity.MedicalRecord;

public interface MedicalRecordRepository
        extends PagingAndSortingRepository<MedicalRecord, Long>
{
	Page<MedicalRecord> findByPatientId(@Param("patientId") Long patientId,
	                                    Pageable pageable);
}
