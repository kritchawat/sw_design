package com.diary.me.demo.entity;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Patient extends AbstractEntity {

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	private String gender;
	private String address;
	private String email;
	private String phone;
	private String emergency;

	private LocalDate dateOfBirth;

//	@JsonIgnore
//	@RestResource(exported = false)
	@OneToMany(mappedBy = "patient",
	           fetch = FetchType.LAZY,
	           cascade = CascadeType.ALL,
	           orphanRemoval = true)
	private Set<Mood> moods;

//	@JsonIgnore
//	@RestResource(exported = false)
	@OneToMany(mappedBy = "patient",
	           fetch = FetchType.LAZY,
	           cascade = CascadeType.ALL,
	           orphanRemoval = true)
	private Set<Event> events;

//	@JsonIgnore
//	@RestResource(exported = false)
	@OneToMany(mappedBy = "patient",
	           fetch = FetchType.LAZY,
	           cascade = CascadeType.ALL,
	           orphanRemoval = true)
	private Set<Drug> drugs;

//	@JsonIgnore
//	@RestResource(exported = false)
	@OneToMany(mappedBy = "patient",
	           fetch = FetchType.LAZY,
	           cascade = CascadeType.ALL,
	           orphanRemoval = true)
	private Set<MedicalRecord> medicalRecords;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmergency() {
		return emergency;
	}

	public void setEmergency(String emergency) {
		this.emergency = emergency;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Set<Mood> getMoods() {
		return moods;
	}

	public void setMoods(Set<Mood> moods) {
		this.moods = moods;
	}

	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}

	public Set<Drug> getDrugs() {
		return drugs;
	}

	public void setDrugs(Set<Drug> drugs) {
		this.drugs = drugs;
	}

	public Set<MedicalRecord> getMedicalRecords() {
		return medicalRecords;
	}

	public void setMedicalRecords(Set<MedicalRecord> medicalRecords) {
		this.medicalRecords = medicalRecords;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (Objects.isNull(obj) || !(obj instanceof Patient)) { return false; }
		return Objects.equals(id, ((Patient) obj).getId());
	}

}
