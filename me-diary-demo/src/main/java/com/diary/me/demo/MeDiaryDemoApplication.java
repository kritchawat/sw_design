package com.diary.me.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class MeDiaryDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeDiaryDemoApplication.class, args);
	}

}
