package com.diary.me.demo.entity.projection;

import org.springframework.data.rest.core.config.Projection;

import com.diary.me.demo.entity.Patient;

@Projection(name="partial", types= {Patient.class})
public interface PartialPatientProjection {
	
	String getFirstName();
	String getLastName();
	
}
