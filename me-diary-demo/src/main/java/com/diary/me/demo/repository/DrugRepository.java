package com.diary.me.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.diary.me.demo.entity.Drug;

public interface DrugRepository extends PagingAndSortingRepository<Drug, Long> {
	
	Page<Drug> findByPatientId(@Param("patientId") Long patientId,
	                           Pageable pageable);
}
