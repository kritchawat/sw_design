package com.diary.me.demo.entity;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Mood extends AbstractEntity {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(referencedColumnName = "ID", nullable = false)
	private Patient patient;

	private Integer moodiness;
	private Integer anxiety;
	private Integer irritability;
	private Integer sleeping;
	private Integer weight;

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Integer getMoodiness() {
		return moodiness;
	}

	public void setMoodiness(Integer moodiness) {
		this.moodiness = moodiness;
	}

	public Integer getAnxiety() {
		return anxiety;
	}

	public void setAnxiety(Integer anxiety) {
		this.anxiety = anxiety;
	}

	public Integer getIrritability() {
		return irritability;
	}

	public void setIrritability(Integer irritability) {
		this.irritability = irritability;
	}

	public Integer getSleeping() {
		return sleeping;
	}

	public void setSleeping(Integer sleeping) {
		this.sleeping = sleeping;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (Objects.isNull(obj) || !(obj instanceof Mood)) { return false; }
		return Objects.equals(id, ((Mood) obj).getId());
	}

}
